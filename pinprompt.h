/*
 * BananUI user interface for calls
 * Copyright (C) 2021 Affe Null <affenull2345@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _PINPROMPT_H_
#define _PINPROMPT_H_

struct daemon_data;
struct pin_prompt;

struct pin_prompt *pinprompt_create(struct daemon_data *dd);
void pinprompt_setPinId(struct pin_prompt *pp, const char *pinid);
void pinprompt_setTries(struct pin_prompt *pp, int tries);
int pinprompt_getFd(struct pin_prompt *pp);
void pinprompt_destroy(struct pin_prompt *pp);
void pinprompt_handleResponse(struct pin_prompt *pp);

#endif
