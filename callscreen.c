/*
 * BananUI user interface for calls - Call screen
 * Copyright (C) 2021 Affe Null <affenull2345@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <unistd.h>
#include <stdio.h>
#include <time.h>
#include <linux/input-event-codes.h>
#include "callscreen.h"
#include "calls.h"
#include "callUI.h"

static void vibrate(unsigned int milliseconds)
{
	FILE *f = fopen("/sys/class/timed_output/vibrator/enable", "w");
	if(f){
		fprintf(f, "%u", milliseconds);
		fclose(f);
	}
	else {
		fprintf(stderr, "[vibrate] [%u]\n", milliseconds);
	}
}

void updateCallscreen(struct callscreen_data *csdata, enum call_state callstate,
	const char *time, const char *number, const char *name)
{
	csdata->state = callstate;
	bSetTextContent(csdata->statelbl.cont,
			CALLSTATE_TO_STRING(callstate),
			PANGO_WEIGHT_NORMAL, 18);
	bLoadImageFromIcon(csdata->icon.cont,
			CALLSTATE_TO_ICON(callstate), 128);
	if(time) bSetTextContent(csdata->timelbl.cont, time,
			PANGO_WEIGHT_NORMAL, 18);
	if(number) bSetTextContent(csdata->numlbl.cont, number,
			PANGO_WEIGHT_NORMAL, 20);
	if(name) bSetTextContent(csdata->namelbl.cont, name,
			PANGO_WEIGHT_BOLD, 20);
	switch(callstate){
		case CALL_STATE_INCOMING:
			vibrate(400);
			bSetSoftkeyText(csdata->view->sk,
					"Accept", "", "Reject");
			break;
		case CALL_STATE_FAILED:
			bSetSoftkeyText(csdata->view->sk, "", "CLOSE", "");
			break;
		case CALL_STATE_ONGOING:
			bSetSoftkeyText(csdata->view->sk,
					csdata->muted ? "Unmute" : "Mute",
					csdata->speaker ? "EAR" : "SPEAKER",
					"");
			break;
		default:
			bSetSoftkeyText(csdata->view->sk, "", "", "");
	}
	bRedrawWindow(csdata->wnd);
}


static int callscreenCallback(void *param, void *data)
{
	struct callscreen_data *csdata = data;
	xkb_keysym_t *sym = param;
	if(*sym == BANANUI_KEY_SoftRight &&
		csdata->state == CALL_STATE_INCOMING)
	{
		fprintf(stderr, "[CS] reject\n");
		rejectIncomingCall(csdata->dd);
	}
	else if(*sym == XKB_KEY_Escape || *sym == XKB_KEY_BackSpace){
		fprintf(stderr, "[CS] hangup\n");
		if(csdata->state == CALL_STATE_INCOMING)
			rejectIncomingCall(csdata->dd);
		else if(csdata->state != CALL_STATE_FAILED)
			hangupCall(csdata->dd);
	}
	else if((*sym == BANANUI_KEY_Call || *sym == BANANUI_KEY_SoftLeft) &&
		csdata->state == CALL_STATE_INCOMING)
	{
		fprintf(stderr, "[CS] answer\n");
		acceptIncomingCall(csdata->dd);
	}
	else if(*sym == BANANUI_KEY_SoftLeft &&
		csdata->state == CALL_STATE_ONGOING)
	{
		if(csdata->muted){
			csdata->muted = 0;
			fprintf(stderr, "[CS] unmute\n");
			unmuteCall(csdata->dd);
			bSetSoftkeyText(csdata->view->sk, "Mute", NULL, NULL);
			bRedrawWindow(csdata->wnd);
		}
		else {
			csdata->muted = 1;
			fprintf(stderr, "[CS] mute\n");
			muteCall(csdata->dd);
			bSetSoftkeyText(csdata->view->sk, "Unmute", NULL, NULL);
			bRedrawWindow(csdata->wnd);
		}
	}
	else if(*sym == XKB_KEY_Return &&
		csdata->state == CALL_STATE_ONGOING)
	{
		if(csdata->speaker){
			csdata->speaker = 0;
			fprintf(stderr, "[CS] unspeaker\n");
			earpieceCall(csdata->dd);
			bSetSoftkeyText(csdata->view->sk,
					NULL, "SPEAKER", NULL);
			bRedrawWindow(csdata->wnd);
		}
		else {
			csdata->speaker = 1;
			fprintf(stderr, "[CS] speaker\n");
			speakerCall(csdata->dd);
			bSetSoftkeyText(csdata->view->sk,
					NULL, "EAR", NULL);
			bRedrawWindow(csdata->wnd);
		}
	}
	return 1;
}

int showCallscreen(struct daemon_data *dd, struct callscreen_data *csdata,
	const char *number, const char *name, enum call_state callstate)
{
	struct sbItem widgets;
	fprintf(stderr, "[CS] start\n");
	bGlobalAppClass = "phone";
	csdata->muted = csdata->speaker = 0;
	csdata->dd = dd;
	csdata->wnd = bCreateWindow(TITLE_PREFIX_ALERT_SCREEN "Call");
	if(!csdata->wnd) return -1;
	csdata->view = bCreateView(0);
	if(!csdata->view) return -1;
	csdata->view->sk = bCreateSoftkeyPanel();
	sbDefaultFgColor = bColorFromTheme("callscreen:text");
	sbDefaultBgColor = bRGBA(0.0, 0.0, 0.0, 0.0);
	csdata->view->bg->color = bColorFromTheme("callscreen:background");
	widgets = callUI(
		CALLSTATE_TO_STRING(callstate), CALLSTATE_TO_ICON(callstate),
		number, name,
		&csdata->statelbl, &csdata->icon, &csdata->timelbl,
		&csdata->numlbl, &csdata->namelbl);
	if(widgets.type != SB_ITEM_BOX) return -1;
	bAddBoxToBox(csdata->view->body, widgets.box);
	switch(callstate){
		case CALL_STATE_INCOMING:
			bSetSoftkeyText(csdata->view->sk,
					"Accept", "", "Reject");
			vibrate(400);
			break;
		case CALL_STATE_FAILED:
			bSetSoftkeyText(csdata->view->sk, "", "CLOSE", "");
			break;
		default:
			break;
	}
	bRegisterEventHandler(&csdata->wnd->keydown,
			callscreenCallback, csdata);
	bShowView(csdata->wnd, csdata->view);
	bRedrawWindow(csdata->wnd);
	return 0;
}

void destroyCallscreen(struct callscreen_data *csdata)
{
	fprintf(stderr, "[CS] destroy\n");
	bDestroyView(csdata->view);
	bDestroyWindow(csdata->wnd);
}

int callscreen_getRingTimeout(struct callscreen_data *csdata,
	struct timespec *timeout)
{
	if(csdata->state != CALL_STATE_INCOMING) return 0;
	timeout->tv_sec = 3;
	timeout->tv_nsec = 0;
	return 1;
}

void callscreen_handleRingTimeout(struct callscreen_data *csdata)
{
	if(csdata->state != CALL_STATE_INCOMING) return;
	vibrate(400);
}

int callscreen_handleResponse(struct callscreen_data *csdata, int fd)
{
	bHandleWindowEvent(csdata->wnd);
	return -1;
}

void callscreen_getConnectionFds(struct callscreen_data *csdata,
	int *fds, size_t *numfds)
{
	*numfds = 1;
	*fds = bGetWindowFd(csdata->wnd);
}
