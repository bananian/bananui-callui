#!/bin/sh

confdir=$HOME/.config/bananui
ringtone=$confdir/ringtone

mkdir -p "$confdir"
if [ ! -f "$ringtone" ]; then
  echo "/usr/share/bananui/ringer.ogg" > "$ringtone"
fi

while true; do
  paplay "$(cat "$ringtone")"
  sleep 1
done
