/* vim: set sw=2 sts=2 et: */

static struct sbItem callUI(
    const char *state, const char *iconame,
    const char *number, const char *name,
    struct sbItem *st, struct sbItem *ico, struct sbItem *tm,
    struct sbItem *num, struct sbItem *nam)
{
  return sbBOX(B_ALIGN_CENTER, B_BOX_VBOX, -1, -1,
    sbBOX(B_ALIGN_START, B_BOX_VBOX, 0, 20, sbEND),
    sbICON(B_ALIGN_CENTER, iconame, 128,
      sbREF(ico), sbEND),
    sbBOX(B_ALIGN_START, B_BOX_HBOX, -1, 25,
      sbMGNL(4.0),
      sbMGNR(4.0),
      sbTEXT(B_ALIGN_START, name ? name : "", PANGO_WEIGHT_BOLD, 20,
        sbREF(nam), sbEND),
      sbEND
    ),
    sbBOX(B_ALIGN_START, B_BOX_HBOX, -1, 25,
      sbMGNL(4.0),
      sbMGNR(4.0),
      sbTEXT(B_ALIGN_START, number ? number : "", PANGO_WEIGHT_NORMAL, 20,
        sbREF(num), sbEND),
      sbEND
    ),
    sbBOX(B_ALIGN_CENTER, B_BOX_HBOX, -1, -1,
      sbBOX(B_ALIGN_CENTER, B_BOX_VBOX, -1, 0,
        sbTEXT(B_ALIGN_CENTER, state, PANGO_WEIGHT_NORMAL, 18,
          sbREF(st), sbEND),
        sbTEXT(B_ALIGN_CENTER, "", PANGO_WEIGHT_NORMAL, 18,
          sbREF(tm), sbEND),
        sbEND
      ),
      sbEND
    ),
    sbEND
  );
}
