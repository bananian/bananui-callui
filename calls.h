/*
 * BananUI user interface for calls
 * Copyright (C) 2021 Affe Null <affenull2345@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _CALLS_H_
#define _CALLS_H_

struct daemon_data;

enum call_state {
	CALL_STATE_INCOMING,
	CALL_STATE_ONGOING,
	CALL_STATE_ENDED,
	CALL_STATE_DIALING,
	CALL_STATE_RINGING,
	CALL_STATE_WAITING,
	CALL_STATE_HELD,
	CALL_STATE_FAILED
};
#define CALLSTATE_TO_STRING(_cs) \
	((_cs) == CALL_STATE_INCOMING ? "Incoming" : \
	 (_cs) == CALL_STATE_ONGOING ? "Active" : \
	 (_cs) == CALL_STATE_ENDED ? "Ended" : \
	 (_cs) == CALL_STATE_DIALING ? "Dialing" : \
	 (_cs) == CALL_STATE_RINGING ? "Ringing" : \
	 (_cs) == CALL_STATE_WAITING ? "Waiting" : \
	 (_cs) == CALL_STATE_HELD ? "On hold" : "Failed")

#define CALLSTATE_TO_ICON(_cs) \
	((_cs) == CALL_STATE_ENDED ? "call-stop-symbolic" : \
	 (_cs) == CALL_STATE_FAILED ? "call-stop-symbolic" : \
	 "call-start-symbolic")

void handleCallCreate(struct daemon_data *dd, const char *number,
	const char *name, enum call_state stat);
void setCallNumber(struct daemon_data *dd, const char *number);
void setCallName(struct daemon_data *dd, const char *name);
void setCallState(struct daemon_data *dd, enum call_state stat);
void rejectIncomingCall(struct daemon_data *dd);
void acceptIncomingCall(struct daemon_data *dd);
void hangupCall(struct daemon_data *dd);
void handleCallDestroy(struct daemon_data *dd);
void muteCall(struct daemon_data *dd);
void unmuteCall(struct daemon_data *dd);
void speakerCall(struct daemon_data *dd);
void earpieceCall(struct daemon_data *dd);

void hidePinPrompt(struct daemon_data *dd);
void showPinPrompt(struct daemon_data *dd, const char *pinid);
void pinPromptCallback(struct daemon_data *dd,
		const char *pinid, const char *pin);

#endif
