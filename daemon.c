/*
 * BananUI user interface for calls
 * Copyright (C) 2021 Affe Null <affenull2345@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <libgen.h>
#include <sys/select.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <unistd.h>
#include <time.h>
#include <dbus/dbus.h>

#include "modem-ofono.h"
#include "callscreen.h"
#include "pinprompt.h"


/* Modified version of timersub from the GNU C library */
#define timespecsub(_a, _b, _result)					      \
  do {									      \
    (_result)->tv_sec = (_a)->tv_sec - (_b)->tv_sec;			      \
    (_result)->tv_nsec = (_a)->tv_nsec - (_b)->tv_nsec;			      \
    if ((_result)->tv_nsec < 0) {					      \
      --(_result)->tv_sec;						      \
      (_result)->tv_nsec += 1000000000;					      \
    }									      \
  } while (0)

struct watch_list_item {
	DBusWatch *watch;
	struct watch_list_item *next;
};

struct daemon_data {
	int isCallActive, isCallStarted, minutes, seconds;
	enum call_state state;
	struct callscreen_data csdata;
	struct pin_prompt *pp;
	struct watch_list_item *watchlist;
	struct timespec starttime;
	void *modem_ofono_data;
	DBusConnection *connection;
};

static void audio_route(const char *op)
{
	if(fork() == 0){
		execl("/usr/share/bananui/call-audio.sh", "", op,
			(const char *)NULL);
		perror("exec call-audio.sh failed");
		_exit(0);
	}
	wait(NULL);
}

void speakerCall(struct daemon_data *dd)
{
	audio_route("enable_speaker");
}

void earpieceCall(struct daemon_data *dd)
{
	audio_route("disable_speaker");
}

void muteCall(struct daemon_data *dd)
{
	audio_route("mute_tx");
}

void unmuteCall(struct daemon_data *dd)
{
	audio_route("unmute_tx");
}

void handleCallCreate(struct daemon_data *dd, const char *number,
	const char *name, enum call_state stat)
{
	dd->isCallActive = 1;
	dd->state = stat;
	if(stat == CALL_STATE_ONGOING){
		clock_gettime(CLOCK_MONOTONIC, &dd->starttime);
		dd->isCallStarted = 1;
		dd->minutes = dd->seconds = 0;
		audio_route("audio_route");
	}
	else {
		dd->isCallStarted = 0;
	}
	if(showCallscreen(dd, &dd->csdata, number, name, stat) < 0){
		fprintf(stderr, "Failed to create call screen\n");
		dd->isCallActive = 0;
	}
}
void handleCallDestroy(struct daemon_data *dd)
{
	if(dd->isCallActive){
		dd->isCallActive = dd->isCallStarted = 0;
		if(dd->state != CALL_STATE_ENDED){
			destroyCallscreen(&dd->csdata);
		}
	}
}
void setCallState(struct daemon_data *dd, enum call_state stat)
{
	dd->state = stat;
	if(dd->state == CALL_STATE_ONGOING && !dd->isCallStarted){
		clock_gettime(CLOCK_MONOTONIC, &dd->starttime);
		dd->isCallStarted = 1;
		dd->minutes = dd->seconds = 0;
		audio_route("audio_route");
	}
	else if(dd->state != CALL_STATE_ONGOING && dd->isCallStarted){
		audio_route("audio_unroute");
		dd->isCallStarted = 0;
	}
	if(dd->isCallActive) updateCallscreen(&dd->csdata, stat, NULL, NULL,
		NULL);
}
void setCallNumber(struct daemon_data *dd, const char *number)
{
	if(dd->isCallActive)
		updateCallscreen(&dd->csdata, dd->state, NULL, number, NULL);
}
void setCallName(struct daemon_data *dd, const char *name)
{
	if(dd->isCallActive)
		updateCallscreen(&dd->csdata, dd->state, NULL, NULL, name);
}
void acceptIncomingCall(struct daemon_data *dd)
{
	modem_ofono_acceptIncomingCall(dd, &dd->modem_ofono_data);
}
void rejectIncomingCall(struct daemon_data *dd)
{
	modem_ofono_rejectIncomingCall(dd, &dd->modem_ofono_data);
}
void hangupCall(struct daemon_data *dd)
{
	modem_ofono_hangupCall(dd, &dd->modem_ofono_data);
}
void showPinPrompt(struct daemon_data *dd, const char *pinid)
{
	if(!dd->pp) dd->pp = pinprompt_create(dd);
	if(!dd->pp) return;
	pinprompt_setPinId(dd->pp, pinid);
}
void hidePinPrompt(struct daemon_data *dd)
{
	if(dd->pp) pinprompt_destroy(dd->pp);
	dd->pp = NULL;
}
void pinPromptCallback(struct daemon_data *dd, const char *pinid,
	const char *pin)
{
	modem_ofono_enterPin(&dd->modem_ofono_data, pinid, pin);
}

int mainLoop(struct daemon_data *dd)
{
	fd_set readfds, writefds;
	int fds[16];
	int res, i;
	int needtimeout;
	int mfds, ppfd;
	size_t nfds;
	struct timespec timeout, curtime, timediff, ringtimeout;
	struct watch_list_item *tmp;
	DBusMessage *msg;

	dd->isCallActive = dd->isCallStarted = 0;

	modem_ofono_startup(dd, &dd->modem_ofono_data);

	while(1){
		mfds = 0;
		FD_ZERO(&readfds);
		FD_ZERO(&writefds);
		nfds = sizeof(fds);
		if(dd->isCallActive){
			callscreen_getConnectionFds(&dd->csdata, fds, &nfds);
			for(i = 0; i < nfds; i++){
				FD_SET(fds[i], &readfds);
				if(fds[i] >= mfds) mfds = fds[i]+1;
			}
		}
		for(tmp = dd->watchlist; tmp; tmp = tmp->next){
			int fd;
			unsigned int flags;
			if(!dbus_watch_get_enabled(tmp->watch)) continue;
			fd = dbus_watch_get_unix_fd(tmp->watch);
			if(fd < 0) continue;
			flags = dbus_watch_get_flags(tmp->watch);
			if(flags & DBUS_WATCH_READABLE)
				FD_SET(fd, &readfds);
			if(flags & DBUS_WATCH_WRITABLE)
				FD_SET(fd, &writefds);
			if(fd >= mfds) mfds = fd+1;
		}

		if(dd->pp){
			ppfd = pinprompt_getFd(dd->pp);
			FD_SET(ppfd, &readfds);
			if(ppfd >= mfds) mfds = ppfd+1;
		}

		needtimeout = 0;

		if(dd->isCallActive && dd->state == CALL_STATE_ENDED){
			/*
			 * Sleep here because we don't want any other events
			 * to interfere. It's bad if the call screen stays
			 * open, takes too long to disappear or disappears
			 * too quickly
			 */
			sleep(2);
			dd->isCallActive = 0;
			destroyCallscreen(&dd->csdata);
		}
		if(dd->isCallActive && dd->state == CALL_STATE_ONGOING){
			clock_gettime(CLOCK_MONOTONIC, &curtime);
			timeout.tv_sec = 0;
			timeout.tv_nsec = dd->starttime.tv_nsec -
				curtime.tv_nsec;
			if(timeout.tv_nsec < 0) timeout.tv_nsec += 1000000000;
			if(timeout.tv_nsec == 0) timeout.tv_sec = 1;
			needtimeout = 1;
		}
		if(dd->isCallActive && dd->state == CALL_STATE_INCOMING &&
			callscreen_getRingTimeout(&dd->csdata, &ringtimeout))
		{
			if(needtimeout){
				if(timeout.tv_sec > ringtimeout.tv_sec){
					timeout.tv_sec = ringtimeout.tv_sec;
					timeout.tv_nsec = ringtimeout.tv_nsec;
				}
				else if(timeout.tv_sec == ringtimeout.tv_sec &&
					timeout.tv_nsec < ringtimeout.tv_nsec)
				{
					timeout.tv_nsec = ringtimeout.tv_nsec;
				}
			}
			else {
				timeout = ringtimeout;
				needtimeout = 1;
			}
		}


		res = pselect(mfds, &readfds, &writefds, NULL,
			needtimeout ? &timeout : NULL,
			NULL);

		if(dd->isCallActive && dd->state == CALL_STATE_INCOMING)
			callscreen_handleRingTimeout(&dd->csdata);

		if(res < 0){
			perror("select");
			return 1;
		}

		if(dd->isCallStarted && dd->state == CALL_STATE_ONGOING){
			char timestr[10];
			int newminutes, newseconds;

			clock_gettime(CLOCK_MONOTONIC, &curtime);
			timespecsub(&curtime, &dd->starttime, &timediff);

			newseconds = timediff.tv_sec % 60;
			newminutes = timediff.tv_sec / 60;
			if(newseconds != dd->seconds ||
				newminutes != dd->minutes)
			{
				snprintf(timestr, sizeof(timestr), "%02d:%02d",
					dd->minutes, dd->seconds);
				updateCallscreen(&dd->csdata,CALL_STATE_ONGOING,
					timestr, NULL, NULL);
				dd->minutes = newminutes;
				dd->seconds = newseconds;
			}
		}
		if(res == 0) continue;
		for(tmp = dd->watchlist; tmp; tmp = tmp->next){
			int fd;
			unsigned int flags = 0;
			fd = dbus_watch_get_unix_fd(tmp->watch);
			if(fd < 0) continue;
			if(FD_ISSET(fd, &readfds))
				flags |= DBUS_WATCH_READABLE;
			if(FD_ISSET(fd, &writefds))
				flags |= DBUS_WATCH_WRITABLE;
			if(flags) dbus_watch_handle(tmp->watch, flags);
		}
		if(dd->pp && FD_ISSET(ppfd, &readfds)){
			pinprompt_handleResponse(dd->pp);
		}
		if(dd->isCallActive){
			for(i = 0; i < nfds; i++){
				if(FD_ISSET(fds[i], &readfds)){
					res = callscreen_handleResponse(
						&dd->csdata, fds[i]);
					if(res >= 0)
						return res;
				}
			}
		}

		while((msg = dbus_connection_pop_message(dd->connection))){
			modem_ofono_handleMessage(msg, dd,
				&dd->modem_ofono_data);
			dbus_message_unref(msg);
		}
	}
}

void printUsage(const char *progfullname)
{
	char *progfullname_copy;
	progfullname_copy = strdup(progfullname);

	fprintf(stderr, "Usage: %s [-f]\n", basename(progfullname_copy));
	fprintf(stderr, "Options:\n");
	fprintf(stderr, "  -f    Do not daemonize, run in the foreground.\n");
}

dbus_bool_t watch_add(DBusWatch *watch, void *data)
{
	struct watch_list_item **watchlist = data, **tmp, *wli;
	for(tmp = watchlist; *tmp; tmp = &((*tmp)->next)) {}
	wli = *tmp = malloc(sizeof(struct watch_list_item));
	if(!wli) return FALSE;
	wli->next = NULL;
	wli->watch = watch;
	dbus_watch_set_data(watch, tmp, NULL);
	return TRUE;
}

void watch_remove(DBusWatch *watch, void *data)
{
	struct watch_list_item **item_to_remove, *tmp;
	item_to_remove = dbus_watch_get_data(watch);
	tmp = *item_to_remove;
	*item_to_remove = tmp->next;
	free(tmp);
}

void watchlist_free(void *data)
{
	struct watch_list_item **watchlist = data, *tmp;
	for(tmp = *watchlist; tmp; tmp = tmp->next) free(tmp);
}

int main(int argc, const char **argv)
{
	int fd, i, nofork = 0;
	DBusConnection *connection = NULL;
	DBusError error;
	struct daemon_data dd;

	dd.watchlist = NULL;
	dd.pp = NULL;

	fd = open("/dev/null", O_RDWR);
	for(i = 1; i < argc; i++){
		if(0 == strcmp(argv[i], "-f")){
			nofork = 1;
		}
		else {
			if(argv[i][0] == '-')
				fprintf(stderr, "Unknown option %s\n", argv[i]);
			printUsage(argv[0]);
			return 1;
		}
	}

	fprintf(stderr, "Starting bananui call manager...\n");
	/* Set up... */
	dbus_error_init(&error);
	connection = dbus_bus_get(DBUS_BUS_SYSTEM, &error);
	if(dbus_error_is_set(&error)){
		fprintf(stderr, "Failed to connect to system bus: %s\n",
			error.message);
		dbus_error_free(&error);
		return 1;
	}
	modem_ofono_initializeDBus(connection, &error, &dd,
		&dd.modem_ofono_data);
	if(dbus_error_is_set(&error)){
		fprintf(stderr, "Failed to initialize D-Bus for ofono modem backend: %s\n", error.message);
	}
	dbus_connection_set_watch_functions(connection, watch_add,
		watch_remove, NULL, &dd.watchlist, watchlist_free);

	dd.connection = connection;

	if(!nofork){
		close(0);
		close(1);
		close(2);
		dup2(fd, 0);
		dup2(fd, 1);
		dup2(fd, 2);
		if(fork() == 0){
			setsid();
			if(fork() == 0) mainLoop(&dd);
		}
		exit(0);
	}
	else return mainLoop(&dd);
}
