#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <sys/select.h>
#include <dbus/dbus.h>
#include <bananui/window.h>
#include <bananui/view.h>
#include <bananui/highlevel.h>
#include <bananui/keys.h>
#include "modem-ofono.h"

int dial(DBusConnection *connection, const char *modempath,
	const char *number)
{
	DBusMessage *callmsg;
	DBusMessageIter args;
	DBusError error;
	const char *hide_callerid = "";

	printf("Dialing %s with modem %s...\n", number, modempath);

	dbus_error_init(&error);
	callmsg = dbus_message_new_method_call("org.ofono", modempath,
		"org.ofono.VoiceCallManager", "Dial");
	if(!callmsg){
		fprintf(stderr, "Failed to create call to Dial\n");
		return -1;
	}

	dbus_message_iter_init_append(callmsg, &args);
	dbus_message_iter_append_basic(&args, DBUS_TYPE_STRING, &number);
	dbus_message_iter_append_basic(&args, DBUS_TYPE_STRING, &hide_callerid);

	dbus_connection_send_with_reply_and_block(connection, callmsg, 3000,
		&error);
	if(dbus_error_is_set(&error)){
		fprintf(stderr, "%s: %s\n", modempath, error.message);
		dbus_error_free(&error);
		return -1;
	}
	return 0;
}

/* Return value: -1=error, 0=keep going, 1=dialing */
int checkInterfaceAndDial(DBusConnection *connection, const char *modempath,
	DBusMessageIter *interface, const char *number)
{
	char *interfacename;
	while(1){
		if(dbus_message_iter_get_arg_type(interface) !=
			DBUS_TYPE_STRING)
		{
			return 0;
		}
		dbus_message_iter_get_basic(interface, &interfacename);
		if(0 == strcmp(interfacename, "org.ofono.VoiceCallManager")){
			return dial(connection, modempath, number) < 0 ? -1 : 1;
		}
		if(!dbus_message_iter_next(interface)) break;
	}
	return 0;
}

/* Return value: -1=error, 0=keep going, 1=dialing */
int checkAndDial(DBusConnection *connection, const char *modempath,
	DBusMessageIter *dict, const char *number)
{
	DBusMessageIter dent, keyval, val, interface;
	int res;

	dbus_message_iter_recurse(dict, &dent);
	while(1){
		char *propname;
		if(dbus_message_iter_get_arg_type(&dent) !=
			DBUS_TYPE_DICT_ENTRY)
		{
			fprintf(stderr, "%s: No properties or invalid dict\n",
				modempath);
			return -1;
		}
		dbus_message_iter_recurse(&dent, &keyval);
		if(dbus_message_iter_get_arg_type(&keyval) !=
			DBUS_TYPE_STRING)
		{
			fprintf(stderr, "%s: Property name not string\n",
				modempath);
			return -1;
		}
		dbus_message_iter_get_basic(&keyval, &propname);
		if(!dbus_message_iter_next(&keyval)){
			fprintf(stderr, "%s: %s: No value\n", modempath,
				propname);
			return -1;
		}
		if(dbus_message_iter_get_arg_type(&keyval) !=
			DBUS_TYPE_VARIANT)
		{
			fprintf(stderr, "%s: %s: Value is not a variant\n",
				modempath, propname);
			return -1;
		}
		dbus_message_iter_recurse(&keyval, &val);
		if(0 == strcmp(propname, "Interfaces")){
			if(dbus_message_iter_get_arg_type(&val) !=
				DBUS_TYPE_ARRAY)
			{
				fprintf(stderr,
					"%s: Interfaces property not array\n",
					modempath);
				return -1;
			}
			dbus_message_iter_recurse(&val, &interface);
			res = checkInterfaceAndDial(connection, modempath,
				&interface, number);
			if(res < 0) return -1;
			else if(res) return 1;
		}
		if(!dbus_message_iter_next(&dent)) break;
	}
	return 0;
}

static int dialNumber(const char *number)
{
	DBusError error;
	DBusConnection *connection;
	DBusMessage *callmsg, *replymsg;
	DBusMessageIter reply, replyarray, modem;
	int res = 0;

	dbus_error_init(&error);
	connection = dbus_bus_get(DBUS_BUS_SYSTEM, &error);
	if(dbus_error_is_set(&error)){
		fprintf(stderr, "Failed to connect to system bus: %s\n",
			error.message);
		dbus_error_free(&error);
		return -1;
	}
	callmsg = dbus_message_new_method_call("org.ofono", "/",
		"org.ofono.Manager", "GetModems");
	if(!callmsg){
		fprintf(stderr, "Couldn't create call to GetModems\n");
		return -1;
	}
	replymsg = dbus_connection_send_with_reply_and_block(
		connection, callmsg, 3000, &error);
	if(dbus_error_is_set(&error)){
		fprintf(stderr, "Failed to get modem list: %s\n",
			error.message);
		dbus_error_free(&error);
		return -1;
	}
	if(!replymsg || !dbus_message_iter_init(replymsg, &reply)){
		fprintf(stderr, "No reply to GetModems!\n");
	}
	if(dbus_message_iter_get_arg_type(&reply) !=
		DBUS_TYPE_ARRAY)
	{
		fprintf(stderr, "Reply from GetModems not an array!\n");
	}
	dbus_message_iter_recurse(&reply, &replyarray);

	while(1){
		char *path;
		if(dbus_message_iter_get_arg_type(&replyarray) !=
			DBUS_TYPE_STRUCT)
		{
			fprintf(stderr, "No modems or invalid reply\n");
			return -1;
		}
		dbus_message_iter_recurse(&replyarray, &modem);
		if(dbus_message_iter_get_arg_type(&modem) !=
			DBUS_TYPE_OBJECT_PATH)
		{
			fprintf(stderr, "First field of modem struct is not an object path\n");
			return -1;
		}
		dbus_message_iter_get_basic(&modem, &path);
		if(!dbus_message_iter_next(&modem)){
			fprintf(stderr, "Modem has no properties!\n");
			if(!dbus_message_iter_next(&replyarray)) break;
			continue;
		}
		if(dbus_message_iter_get_arg_type(&modem) !=
			DBUS_TYPE_ARRAY ||
			dbus_message_iter_get_element_type(&modem) !=
			DBUS_TYPE_DICT_ENTRY)
		{
			fprintf(stderr,
				"Modem properties field is not a dict\n");
			return -1;
		}

		res = checkAndDial(connection, path, &modem, number);
		if(res > 0) return 0;

		if(!dbus_message_iter_next(&replyarray)) break;
	}
	fprintf(stderr, "No modems found\n");
	return -1;
}

struct bananui_dialer {
	bWindow *wnd;
	bView *view;
	bBoxWidget *header;
	bContentWidget *inp;
	bContentWidget *status;
	int running;
	char number[1024];
};

static void delete_digit(struct bananui_dialer *dialer)
{
	int i = dialer->inp->cursor;
	if(i == 0) return;
	for(; dialer->number[i-1]; i++){
		dialer->number[i-1] = dialer->number[i];
	}
	dialer->inp->cursor--;
	bSetTextContent(dialer->inp, dialer->number, PANGO_WEIGHT_NORMAL, 20);
	bRedrawWindow(dialer->wnd);
}

static void insert_common(struct bananui_dialer *dialer, char ch)
{
	int i;
	size_t len = strlen(dialer->number);
	if(len + 1 == sizeof(dialer->number)) return;
	for(i = len; i >= dialer->inp->cursor; i--){
		dialer->number[i+1] = dialer->number[i];
	}
	dialer->number[dialer->inp->cursor] = ch;
	dialer->inp->cursor++;
}

static void insert_digit(struct bananui_dialer *dialer, char ch)
{
	insert_common(dialer, ch);
	bSetTextContent(dialer->inp, dialer->number, PANGO_WEIGHT_NORMAL, 20);
	bRedrawWindow(dialer->wnd);
}

enum insert_replace {
	INSERT_NO_REPLACE,
	INSERT_REPLACE_NEXT,
	INSERT_REPLACED
};

static void insert_alt(struct bananui_dialer *dialer, char *ch)
{
	char *tmp;
	int i = dialer->inp->cursor - 1;
	enum insert_replace replace = INSERT_NO_REPLACE;
	if(i >= 0){
		for(tmp = ch; *tmp; tmp++){
			if(dialer->number[i] == *tmp){
				replace = INSERT_REPLACE_NEXT;
			}
			else if(replace){
				dialer->number[i] = *tmp;
				replace = INSERT_REPLACED;
				break;
			}
		}
	}
	switch(replace){
		case INSERT_NO_REPLACE:
			insert_common(dialer, *ch);
			break;
		case INSERT_REPLACE_NEXT:
			dialer->number[i] = *ch;
			break;
		default:
			break;
	}
	bSetTextContent(dialer->inp, dialer->number, PANGO_WEIGHT_NORMAL, 20);
	bRedrawWindow(dialer->wnd);
}

static int keydown_handler(void *param, void *data)
{
	struct bananui_dialer *dialer = data;
	xkb_keysym_t *sym = param;

	switch(*sym){
		case BANANUI_KEY_Call:
		case XKB_KEY_Return:
			if(dialNumber(dialer->number) < 0){
				bSetTextContent(dialer->status,
					"Operation failed",
					PANGO_WEIGHT_BOLD, 16);
				bRedrawWindow(dialer->wnd);
			}
			else {
				dialer->running = 0;
			}
			break;
		case XKB_KEY_Left:
			if(dialer->inp->cursor > 0)
				dialer->inp->cursor--;
			bRedrawWindow(dialer->wnd);
			break;
		case XKB_KEY_Right:
			if(dialer->number[dialer->inp->cursor])
				dialer->inp->cursor++;
			bRedrawWindow(dialer->wnd);
			break;
		case XKB_KEY_BackSpace:
			if(dialer->number[0]){
				delete_digit(dialer);
				break;
			}
			/* fallthrough */
		case XKB_KEY_Escape:
			dialer->running = 0;
			break;
		case XKB_KEY_1: insert_digit(dialer, '1'); break;
		case XKB_KEY_2: insert_digit(dialer, '2'); break;
		case XKB_KEY_3: insert_digit(dialer, '3'); break;
		case XKB_KEY_4: insert_digit(dialer, '4'); break;
		case XKB_KEY_5: insert_digit(dialer, '5'); break;
		case XKB_KEY_6: insert_digit(dialer, '6'); break;
		case XKB_KEY_7: insert_digit(dialer, '7'); break;
		case XKB_KEY_8: insert_digit(dialer, '8'); break;
		case XKB_KEY_9: insert_digit(dialer, '9'); break;
		case BANANUI_KEY_Star: insert_alt(dialer, "*+"); break;
		case XKB_KEY_0: insert_digit(dialer, '0'); break;
		case BANANUI_KEY_Pound: insert_digit(dialer, '#'); break;
		default:
			break;
	}
	return 1;
}

int main(int argc, const char **argv)
{
	bBoxWidget *inpbox;
	struct bananui_dialer dialer;
	if(argc < 2){
		fprintf(stderr, "Usage: %s phonenumber\n", argv[0]);
		return 1;
	}
	bGlobalAppClass = "phone:dialer";
	dialer.running = 1;
	dialer.wnd = bCreateSizedWindow(TITLE_PREFIX_ALERT_SCREEN "Dialer",
			0, 120);
	if(!dialer.wnd) return 1;
	dialer.view = bCreateView(0);
	if(!dialer.view) return 1;
	dialer.view->bg->color = bColorFromTheme("background");

	dialer.header = bCreateBoxWidget(B_ALIGN_START, B_BOX_VBOX, -1, 30);
	if(!dialer.header) return 1;
	bAddBoxToBox(dialer.view->body, dialer.header);
	dialer.header->color = bRGBA(0.0, 0.0, 0.0, 1.0);

	strncpy(dialer.number, argv[1], sizeof(dialer.number)-1);
	dialer.number[sizeof(dialer.number)-1] = '\0';

	inpbox = bCreateBoxWidget(B_ALIGN_CENTER, B_BOX_VBOX, -1, -1);
	if(!inpbox) return 1;
	bAddBoxToBox(dialer.view->body, inpbox);
	inpbox->color = bRGBA(0.0, 0.0, 0.0, 0.0);
	inpbox->mgn_l = inpbox->mgn_r = 5;

	dialer.inp = bCreateContentWidget(B_ALIGN_CENTER, 0);
	if(!dialer.inp) return 1;
	bAddContentToBox(inpbox, dialer.inp);
	dialer.inp->cursor = strlen(dialer.number);
	dialer.inp->cursor_visible = 1;
	dialer.inp->color = bColorFromTheme("text");
	bSetTextContent(dialer.inp, dialer.number, PANGO_WEIGHT_NORMAL, 20);

	dialer.status = bCreateContentWidget(B_ALIGN_CENTER, 0);
	if(!dialer.status) return 1;
	bAddContentToBox(dialer.view->body, dialer.status);
	dialer.status->color = dialer.inp->color;

	bRegisterEventHandler(&dialer.wnd->keydown, keydown_handler, &dialer);

	bShowView(dialer.wnd, dialer.view);
	bRedrawWindow(dialer.wnd);

	while(dialer.running && !dialer.wnd->closing){
		int res, fd;
		fd_set fds;

		fd = bGetWindowFd(dialer.wnd);

		FD_ZERO(&fds);
		FD_SET(fd, &fds);

		res = select(fd+1, &fds, NULL, NULL, NULL);

		if(res < 0 && errno != EINTR){
			perror("select");
			return 1;
		}
		if(res > 0){
			if(FD_ISSET(fd, &fds)){
				bHandleWindowEvent(dialer.wnd);
			}
		}
	}
	return 0;
}
