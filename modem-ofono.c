/*
 * BananUI user interface for calls - oFono backend
 * Copyright (C) 2021 Affe Null <affenull2345@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <string.h>
#include <stdio.h>
#include <dbus/dbus.h>
#include <stdlib.h>
#include "calls.h"
#include "modem-ofono.h"

struct ofono_modem_data {
	char *callpath, *path;
	DBusConnection *conn;
};

#define OFONO_STATE_TO_CS(_os) \
	(0 == strcmp((_os), "active") ? CALL_STATE_ONGOING : \
	 0 == strcmp((_os), "held") ? CALL_STATE_HELD : \
	 0 == strcmp((_os), "dialing") ? CALL_STATE_DIALING : \
	 0 == strcmp((_os), "alerting") ? CALL_STATE_RINGING : \
	 0 == strcmp((_os), "incoming") ? CALL_STATE_INCOMING : \
	 0 == strcmp((_os), "waiting") ? CALL_STATE_WAITING : \
	 CALL_STATE_ENDED)

/* This will initialize the data structures */
void modem_ofono_initializeDBus(DBusConnection *connection, DBusError *error,
	struct daemon_data *dd, void **_modem_data)
{
	struct ofono_modem_data *modem_data;
	dbus_bus_add_match(connection, "type='signal',interface='org.ofono.VoiceCallManager'", error);
	dbus_bus_add_match(connection, "type='signal',interface='org.ofono.SimManager'", error);
	dbus_bus_add_match(connection, "type='signal',interface='org.ofono.VoiceCall'", error);
	dbus_connection_flush(connection);
	modem_data = *_modem_data = malloc(sizeof(struct ofono_modem_data));
	modem_data->callpath = NULL;
	modem_data->conn = connection;
}

static void setModemProperty(struct ofono_modem_data *modem_data,
	const char *path, const char *name, int type, const void *value)
{
	char signature[2];
	DBusMessage *call, *reply;
	DBusMessageIter args, val;
	DBusError error;

	signature[0] = (char)type;
	signature[1] = '\0';
	dbus_error_init(&error);

	call = dbus_message_new_method_call("org.ofono", path,
		"org.ofono.Modem", "SetProperty");
	if(!call){
		fprintf(stderr,
			"Failed to set property: couldn't create message\n");
		return;
	}
	dbus_message_iter_init_append(call, &args);
	if(!dbus_message_iter_append_basic(&args, DBUS_TYPE_STRING, &name))
	{
		fprintf(stderr, "Failed to set property: Out of memory\n");
		return;
	}
	if(!dbus_message_iter_open_container(&args, DBUS_TYPE_VARIANT,
		signature, &val))
	{
		fprintf(stderr, "Failed to set property: Out of memory\n");
		return;
	}
	if(!dbus_message_iter_append_basic(&val, type, value))
	{
		fprintf(stderr, "Failed to set property: Out of memory\n");
		return;
	}
	if(!dbus_message_iter_close_container(&args, &val))
	{
		fprintf(stderr, "Failed to set property: Out of memory\n");
		return;
	}
	reply = dbus_connection_send_with_reply_and_block(
		modem_data->conn, call, 3000, &error);
	if(dbus_error_is_set(&error)){
		fprintf(stderr, "Failed to set property %s of %s: %s\n",
			path, name, error.message);
		dbus_error_free(&error);
		return;
	}
	if(!reply){
		fprintf(stderr, "No reply for SetProperty\n");
	}
}

static void handleModemProperties(struct ofono_modem_data *modem_data,
	DBusMessageIter *dict)
{
	DBusMessageIter dent, keyval, val;
	dbus_bool_t powered = FALSE, online = FALSE;

	dbus_message_iter_recurse(dict, &dent);
	while(1){
		char *propname;
		if(dbus_message_iter_get_arg_type(&dent) !=
			DBUS_TYPE_DICT_ENTRY)
		{
			fprintf(stderr, "%s: No properties or invalid dict\n",
				modem_data->path);
			return;
		}
		dbus_message_iter_recurse(&dent, &keyval);
		if(dbus_message_iter_get_arg_type(&keyval) !=
			DBUS_TYPE_STRING)
		{
			fprintf(stderr, "%s: Property name not string\n",
				modem_data->path);
			return;
		}
		dbus_message_iter_get_basic(&keyval, &propname);
		if(!dbus_message_iter_next(&keyval)){
			fprintf(stderr, "%s: %s: No value\n",
				modem_data->path, propname);
			return;
		}
		if(dbus_message_iter_get_arg_type(&keyval) !=
			DBUS_TYPE_VARIANT)
		{
			fprintf(stderr, "%s: %s: Value is not a variant\n",
				modem_data->path, propname);
			return;
		}
		dbus_message_iter_recurse(&keyval, &val);
		if(0 == strcmp(propname, "Powered")){
			if(dbus_message_iter_get_arg_type(&val) !=
				DBUS_TYPE_BOOLEAN)
			{
				fprintf(stderr,
					"%s: Powered property not boolean\n",
					modem_data->path);
				return;
			}
			dbus_message_iter_get_basic(&val, &powered);
			fprintf(stderr, "%s: Powered: %s\n", modem_data->path,
				powered ? "yes" : "no");
		}
		else if(0 == strcmp(propname, "Online")){
			if(dbus_message_iter_get_arg_type(&val) !=
				DBUS_TYPE_BOOLEAN)
			{
				fprintf(stderr,
					"%s: Online property not boolean\n",
					modem_data->path);
				return;
			}
			dbus_message_iter_get_basic(&val, &online);
			fprintf(stderr, "%s: Online: %s\n", modem_data->path,
				online ? "yes" : "no");
		}
		if(!dbus_message_iter_next(&dent)) break;
	}
	if(!powered){
		powered = TRUE;
		setModemProperty(modem_data, modem_data->path,
			"Powered", DBUS_TYPE_BOOLEAN, &powered);
	}
	if(!online){
		online = TRUE;
		setModemProperty(modem_data, modem_data->path,
			"Online", DBUS_TYPE_BOOLEAN, &online);
	}
}

/* and this will start up the modem. */
void modem_ofono_startup(struct daemon_data *dd, void **_modem_data)
{
	struct ofono_modem_data *modem_data = *_modem_data;
	DBusMessage *callmsg, *replymsg;
	DBusMessageIter reply, replyarray, modem;
	DBusError error;

	dbus_error_init(&error);
	callmsg = dbus_message_new_method_call("org.ofono", "/",
		"org.ofono.Manager", "GetModems");
	if(!callmsg){
		fprintf(stderr, "Couldn't create call to GetModems\n");
		return;
	}
	replymsg = dbus_connection_send_with_reply_and_block(
		modem_data->conn, callmsg, 3000, &error);
	if(dbus_error_is_set(&error)){
		fprintf(stderr, "Failed to get modem list: %s\n",
			error.message);
		dbus_error_free(&error);
		return;
	}
	if(!replymsg || !dbus_message_iter_init(replymsg, &reply)){
		fprintf(stderr, "No reply to GetModems!\n");
	}
	if(dbus_message_iter_get_arg_type(&reply) !=
		DBUS_TYPE_ARRAY)
	{
		fprintf(stderr, "Reply from GetModems not an array!\n");
	}
	dbus_message_iter_recurse(&reply, &replyarray);

	while(1){
		if(dbus_message_iter_get_arg_type(&replyarray) !=
			DBUS_TYPE_STRUCT)
		{
			fprintf(stderr, "No modems or invalid reply\n");
			return;
		}
		dbus_message_iter_recurse(&replyarray, &modem);
		if(dbus_message_iter_get_arg_type(&modem) !=
			DBUS_TYPE_OBJECT_PATH)
		{
			fprintf(stderr, "First field of modem struct is not an object path\n");
			return;
		}
		dbus_message_iter_get_basic(&modem, &modem_data->path);
		if(!dbus_message_iter_next(&modem)){
			fprintf(stderr, "Modem has no properties!\n");
			if(!dbus_message_iter_next(&replyarray)) break;
			continue;
		}
		if(dbus_message_iter_get_arg_type(&modem) !=
			DBUS_TYPE_ARRAY ||
			dbus_message_iter_get_element_type(&modem) !=
			DBUS_TYPE_DICT_ENTRY)
		{
			fprintf(stderr,
				"Modem properties field is not a dict\n");
			return;
		}
		handleModemProperties(modem_data, &modem);

		if(!dbus_message_iter_next(&replyarray)) break;
	}
}

void modem_ofono_handleMessage(DBusMessage *msg, struct daemon_data *dd,
	void **_modem_data)
{
	struct ofono_modem_data *modem_data = *_modem_data;
	DBusMessageIter args, props, keyval, val;
	char *propname, *number = NULL, *state = NULL, *name = NULL;
	if(dbus_message_is_signal(msg, "org.ofono.VoiceCallManager",
			"CallAdded"))
	{
		if(modem_data->callpath){
			fprintf(stderr, "Multiple calls not supported!\n");
			return;
		}
		fprintf(stderr, "Call added\n");
		if(!dbus_message_iter_init(msg, &args)){
			fprintf(stderr, "No argument from CallAdded\n");
			return;
		}
		if(dbus_message_iter_get_arg_type(&args) !=
			DBUS_TYPE_OBJECT_PATH)
		{
			fprintf(stderr,
				"CallAdded arg #1 is not an object path\n");
			return;
		}
		dbus_message_iter_get_basic(&args, &modem_data->callpath);

		/* Copy the string that D-Bus gave us */
		modem_data->callpath = strdup(modem_data->callpath);

		fprintf(stderr, "Call path: %s\n", modem_data->callpath);
		if(!dbus_message_iter_next(&args)){
			fprintf(stderr,
				"No properties dict from CallAdded\n");
			return;
		}
		if(dbus_message_iter_get_arg_type(&args) !=
			DBUS_TYPE_ARRAY ||
			dbus_message_iter_get_element_type(&args) !=
			DBUS_TYPE_DICT_ENTRY)
		{
			fprintf(stderr,
				"CallAdded arg #2 is not a dict\n");
			return;
		}
		dbus_message_iter_recurse(&args, &props);
		while(1){
			if(dbus_message_iter_get_arg_type(&props) !=
				DBUS_TYPE_DICT_ENTRY)
			{
				fprintf(stderr,
					"Dict contains no dict entry\n");
				return;
			}
			dbus_message_iter_recurse(&props, &keyval);
			if(dbus_message_iter_get_arg_type(&keyval) !=
				DBUS_TYPE_STRING)
			{
				fprintf(stderr,
					"Non-string property name in dict\n");
				return;
			}
			dbus_message_iter_get_basic(&keyval, &propname);
			if(!dbus_message_iter_next(&keyval)){
				fprintf(stderr, "No value for %s\n", propname);
				return;
			}
			if(dbus_message_iter_get_arg_type(&keyval) !=
				DBUS_TYPE_VARIANT)
			{
				fprintf(stderr, "Value is not a variant\n");
				return;
			}
			dbus_message_iter_recurse(&keyval, &val);
			if(0 == strcmp(propname, "LineIdentification")){
				if(dbus_message_iter_get_arg_type(&val) !=
					DBUS_TYPE_STRING)
				{
					fprintf(stderr,
						"Number is not a string\n");
					if(!dbus_message_iter_next(&props))
						break;
					continue;
				}
				dbus_message_iter_get_basic(&val, &number);
				fprintf(stderr, "Number: %s\n", number);
			}
			else if(0 == strcmp(propname, "Name")){
				if(dbus_message_iter_get_arg_type(&val) !=
					DBUS_TYPE_STRING)
				{
					fprintf(stderr,
						"Name is not a string\n");

					if(!dbus_message_iter_next(&props))
						break;
				}
				dbus_message_iter_get_basic(&val, &name);
				fprintf(stderr, "Name: %s\n", name);
			}
			else if(0 == strcmp(propname, "State")){
				if(dbus_message_iter_get_arg_type(&val) !=
					DBUS_TYPE_STRING)
				{
					fprintf(stderr,
						"State is not a string\n");
					if(!dbus_message_iter_next(&props))
						break;
					continue;
				}
				dbus_message_iter_get_basic(&val, &state);
				fprintf(stderr, "State: %s\n", state);
			}
			if(!dbus_message_iter_next(&props)) break;
		}
		if(state && number){
			handleCallCreate(dd, number, name,
				OFONO_STATE_TO_CS(state));
		}
		else {
			fprintf(stderr, "Call has no state or number\n");
		}
	}
	else if(dbus_message_is_signal(msg, "org.ofono.VoiceCallManager",
			"CallRemoved"))
	{
		fprintf(stderr, "Call removed\n");
	}
	else if(dbus_message_is_signal(msg, "org.ofono.SimManager",
			"PropertyChanged"))
	{
		if(!dbus_message_iter_init(msg, &args)){
			fprintf(stderr, "No argument from PropertyChanged\n");
			return;
		}
		if(dbus_message_iter_get_arg_type(&args) !=
			DBUS_TYPE_STRING)
		{
			fprintf(stderr,
				"PropertyChanged arg #1 is not a string\n");
			return;
		}
		dbus_message_iter_get_basic(&args, &propname);
		fprintf(stderr, "SimManager Property changed: %s\n", propname);
		if(!dbus_message_iter_next(&args)){
			fprintf(stderr,
				"No property value from PropertyChanged\n");
			return;
		}
		if(dbus_message_iter_get_arg_type(&args) !=
			DBUS_TYPE_VARIANT)
		{
			fprintf(stderr, "Value is not a variant\n");
			return;
		}
		dbus_message_iter_recurse(&args, &val);
		if(0 == strcmp(propname, "PinRequired")){
			if(dbus_message_iter_get_arg_type(&val) !=
				DBUS_TYPE_STRING)
			{
				fprintf(stderr,
					"PinRequired is not a string\n");
				return;
			}
			dbus_message_iter_get_basic(&val, &state);
			if(0 == strcmp(state, "none")){
				hidePinPrompt(dd);
			}
			else {
				showPinPrompt(dd, state);
			}
		}
		else if(0 == strcmp(propname, "Present")){
			hidePinPrompt(dd);
		}
	}
	else if(dbus_message_is_signal(msg, "org.ofono.VoiceCall",
			"PropertyChanged"))
	{
		const char *sender;
		sender = dbus_message_get_path(msg);
		if(!modem_data->callpath ||
			!sender ||
			0 != strcmp(sender, modem_data->callpath))
		{
			fprintf(stderr,
				"Signal from non-current call: %s != %s\n",
				sender, modem_data->callpath);
			return;
		}

		if(!dbus_message_iter_init(msg, &args)){
			fprintf(stderr, "No argument from PropertyChanged\n");
			return;
		}
		if(dbus_message_iter_get_arg_type(&args) !=
			DBUS_TYPE_STRING)
		{
			fprintf(stderr,
				"PropertyChanged arg #1 is not a string\n");
			return;
		}
		dbus_message_iter_get_basic(&args, &propname);
		fprintf(stderr, "Property changed: %s\n", propname);
		if(!dbus_message_iter_next(&args)){
			fprintf(stderr,
				"No property value from PropertyChanged\n");
			return;
		}
		if(dbus_message_iter_get_arg_type(&args) !=
			DBUS_TYPE_VARIANT)
		{
			fprintf(stderr, "Value is not a variant\n");
			return;
		}
		dbus_message_iter_recurse(&args, &val);
		if(0 == strcmp(propname, "State")){
			if(dbus_message_iter_get_arg_type(&val) !=
				DBUS_TYPE_STRING)
			{
				fprintf(stderr, "State is not a string\n");
				return;
			}
			dbus_message_iter_get_basic(&val, &state);
			setCallState(dd, OFONO_STATE_TO_CS(state));
			if(0 == strcmp(state, "disconnected")){
				fprintf(stderr, "Call disconnected\n");
				if(modem_data->callpath){
					free(modem_data->callpath);
					modem_data->callpath = NULL;
				}
			}
		}
		else if(0 == strcmp(propname, "Name")){
			if(dbus_message_iter_get_arg_type(&val) !=
				DBUS_TYPE_STRING)
			{
				fprintf(stderr, "Name is not a string\n");
				return;
			}
			dbus_message_iter_get_basic(&val, &name);
			setCallName(dd, name);
		}
		else if(0 == strcmp(propname, "LineIdentification")){
			if(dbus_message_iter_get_arg_type(&val) !=
				DBUS_TYPE_STRING)
			{
				fprintf(stderr, "Number is not a string\n");
				return;
			}
			dbus_message_iter_get_basic(&val, &number);
			setCallNumber(dd, number);
		}
	}
}
void modem_ofono_acceptIncomingCall(struct daemon_data *dd, void **_modem_data)
{
	struct ofono_modem_data *modem_data = *_modem_data;
	DBusMessage *callmsg;
	DBusError error;

	if(!modem_data->callpath){
		fprintf(stderr, "No call, can't answer it\n");
		return;
	}

	dbus_error_init(&error);
	callmsg = dbus_message_new_method_call("org.ofono",
		modem_data->callpath, "org.ofono.VoiceCall", "Answer");
	if(!callmsg){
		fprintf(stderr, "Couldn't create call to Answer\n");
		return;
	}
	dbus_connection_send_with_reply_and_block(
		modem_data->conn, callmsg, 3000, &error);
	if(dbus_error_is_set(&error)){
		fprintf(stderr, "Failed to answer call: %s\n",
			error.message);
		dbus_error_free(&error);
		return;
	}
}
void modem_ofono_rejectIncomingCall(struct daemon_data *dd, void **modem_data)
{
	modem_ofono_hangupCall(dd, modem_data);
}
void modem_ofono_hangupCall(struct daemon_data *dd, void **_modem_data)
{
	struct ofono_modem_data *modem_data = *_modem_data;
	DBusMessage *callmsg;
	DBusError error;

	if(!modem_data->callpath){
		fprintf(stderr, "No call, can't hang up\n");
		return;
	}

	dbus_error_init(&error);
	callmsg = dbus_message_new_method_call("org.ofono",
		modem_data->callpath, "org.ofono.VoiceCall", "Hangup");
	if(!callmsg){
		fprintf(stderr, "Couldn't create call to Hangup\n");
		return;
	}
	dbus_connection_send_with_reply_and_block(
		modem_data->conn, callmsg, 3000, &error);
	if(dbus_error_is_set(&error)){
		fprintf(stderr, "Failed to hang up call: %s\n",
			error.message);
		dbus_error_free(&error);
		return;
	}
}
void modem_ofono_enterPin(void **_modem_data,
	const char *pinid, const char *pin)
{
	struct ofono_modem_data *modem_data = *_modem_data;
	DBusMessage *pinmsg;
	DBusMessageIter args;
	DBusError error;
	if(!modem_data->path){
		fprintf(stderr, "No modem, cannot enter pin\n");
		return;
	}
	dbus_error_init(&error);
	pinmsg = dbus_message_new_method_call("org.ofono",
		modem_data->path, "org.ofono.SimManager", "EnterPin");
	if(!pinmsg){
		fprintf(stderr, "Couldn't create call to EnterPin\n");
		return;
	}
	dbus_message_iter_init_append(pinmsg, &args);
	if(!dbus_message_iter_append_basic(&args, DBUS_TYPE_STRING, &pinid))
	{
		fprintf(stderr, "Failed to enter pin: Out of memory\n");
		return;
	}
	if(!dbus_message_iter_append_basic(&args, DBUS_TYPE_STRING, &pin))
	{
		fprintf(stderr, "Failed to enter pin: Out of memory\n");
		return;
	}
	dbus_connection_send_with_reply_and_block(
		modem_data->conn, pinmsg, 3000, &error);
	if(dbus_error_is_set(&error)){
		fprintf(stderr, "Failed to enter pin: %s\n",
			error.message);
		dbus_error_free(&error);
		return;
	}
}
