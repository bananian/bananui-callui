#include <stdlib.h>
#include <stdio.h>
#include <bananui/widget.h>
#include <bananui/highlevel.h>
#include <bananui/view.h>
#include <bananui/window.h>
#include "pinprompt.h"
#include "calls.h"

struct pin_prompt {
	bWindow *wnd;
	bView *view;
	bContentWidget *prompt, *tries;
	bInputWidget *inp;
	char *pinid;
	struct daemon_data *dd;
};

static int pinpromptKeydownHandler(void *param, void *data)
{
	struct pin_prompt *pp = data;
	xkb_keysym_t *sym = param;
	if(*sym == BANANUI_KEY_SoftRight){
		char *text = bGetInputText(pp->inp);
		pinPromptCallback(pp->dd, pp->pinid, text);
		free(text);
	}
	return 1;
}

struct pin_prompt *pinprompt_create(struct daemon_data *dd)
{
	struct pin_prompt *pp;
	bWidgetColor text_color;
	bWindow *wnd;
	bView *view;
	bContentWidget *prompt, *tries;
	bInputWidget *inp;

	bGlobalAppClass = "phone";
	
	wnd = bCreateSizedWindow(TITLE_PREFIX_ALERT_SCREEN "Unlock SIM",
			0, 165);
	if(!wnd) return NULL;
	view = bCreateView(1);
	if(!view) goto free_wnd;

	view->sk = bCreateSoftkeyPanel();
	if(!view->sk) goto free_view_except_sk;
	bSetSoftkeyText(view->sk, "", "", "Enter");

	text_color = bColorFromTheme("text");
	view->bg->color = bColorFromTheme("background");
	bAddFillToBox(view->body);

	view->header = bCreateBoxWidget(B_ALIGN_START, B_BOX_VBOX, -1, 30);
	if(!view->header) goto free_view_except_header;
	view->header->color = bRGBA(0.0, 0.0, 0.0, 1.0);

	prompt = bCreateContentWidget(B_ALIGN_START, 0);
	if(!prompt) goto free_view;
	bAddContentToBox(view->body, prompt);
	prompt->color = text_color;

	tries = bCreateContentWidget(B_ALIGN_START, 0);
	if(!tries) goto free_view;
	bAddContentToBox(view->body, tries);
	tries->color = text_color;

	inp = bCreateInputWidget("", wnd, B_INPUT_STYLE_SINGLELINE,
		B_INPUT_MODE_NUMBER);
	if(!inp) goto free_view;
	bAddBoxToBox(view->body, inp->box);

	bShowView(wnd, view);
	bRedrawWindow(wnd);

	pp = calloc(1, sizeof(struct pin_prompt));
	if(!pp) goto free_view;
	pp->dd = dd;
	pp->wnd = wnd;
	pp->view = view;
	pp->prompt = prompt;
	pp->tries = tries;
	pp->inp = inp;

	bRegisterEventHandler(&pp->wnd->keydown, pinpromptKeydownHandler, pp);
	return pp;
free_view:
	bDestroyBoxRecursive(view->header);
free_view_except_sk:
	bDestroySoftkeyPanel(view->sk);
free_view_except_header:
	bDestroyView(view);
free_wnd:
	bDestroyWindow(wnd);
	fprintf(stderr,
		"Out of memory or unknown error while creating pin prompt\n");
	return NULL;
}
void pinprompt_setPinId(struct pin_prompt *pp, const char *pinid){
	char formatted[12];
	free(pp->pinid);
	pp->pinid = strdup(pinid);
	snprintf(formatted, sizeof(formatted), "Enter %s", pinid);
	bSetTextContent(pp->prompt, formatted, PANGO_WEIGHT_BOLD, 16);
	bRedrawWindow(pp->wnd);
}
void pinprompt_setTries(struct pin_prompt *pp, int tries){
	char formatted[14];
	snprintf(formatted, sizeof(formatted), "%d tries left", tries);
	bSetTextContent(pp->tries, formatted, PANGO_WEIGHT_BOLD, 16);
	bRedrawWindow(pp->wnd);
}
int pinprompt_getFd(struct pin_prompt *pp){
	return bGetWindowFd(pp->wnd);
}
void pinprompt_destroy(struct pin_prompt *pp){
	bDestroyBoxRecursive(pp->view->header);
	bDestroySoftkeyPanel(pp->view->sk);
	bDestroyView(pp->view);
	bDestroyWindow(pp->wnd);
	free(pp->pinid);
	free(pp);
}
void pinprompt_handleResponse(struct pin_prompt *pp){
	bHandleWindowEvent(pp->wnd);
}
