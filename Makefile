PKG_CONFIG = pkg-config
CFLAGS += -Wall -g $(shell $(PKG_CONFIG) --cflags dbus-1 libwbananui)
LDFLAGS += $(shell $(PKG_CONFIG) --libs dbus-1)
LDFLAGS_UI += $(shell $(PKG_CONFIG) --libs dbus-1 libwbananui)
OBJECTS = daemon.o modem-ofono.o callscreen.o pinprompt.o
DIALOBJECTS = dial.o
OUTPUTS = bananui-callui bananui-dial

all: $(OUTPUTS)

bananui-callui: $(OBJECTS)
	$(CC) -o $@ $(OBJECTS) $(LDFLAGS_UI) $(CFLAGS)

bananui-dial: $(DIALOBJECTS)
	$(CC) -o $@ $(DIALOBJECTS) $(LDFLAGS) $(CFLAGS)

callscreen.o: callscreen.h callUI.h calls.h
dial.o: calls.h modem-ofono.h
daemon.o: callscreen.h calls.h modem-ofono.h
modem-ofono.o: calls.h modem-ofono.h

%.o: %.c
	$(CC) -o $@ -c $*.c $(CFLAGS)

clean:
	rm -f $(OBJECTS) $(DIALOBJECTS) $(OUTPUTS)

install:
	install $(OUTPUTS) $(DESTDIR)/usr/bin
