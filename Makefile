PKG_CONFIG = pkg-config
CFLAGS += -Wall -g $(shell $(PKG_CONFIG) --cflags dbus-1 libwbananui1)
LDFLAGS += $(shell $(PKG_CONFIG) --libs dbus-1 libwbananui1)
OBJECTS = daemon.o modem-ofono.o callscreen.o pinprompt.o
DIALOBJECTS = dial.o
OUTPUTS = bananui-callui bananui-dialer

all: $(OUTPUTS)

bananui-callui: $(OBJECTS)
	$(CC) -o $@ $(OBJECTS) $(LDFLAGS) $(CFLAGS)

bananui-dialer: $(DIALOBJECTS)
	$(CC) -o $@ $(DIALOBJECTS) $(LDFLAGS) $(CFLAGS)

callscreen.o: callscreen.h callUI.h calls.h
dial.o: calls.h modem-ofono.h
daemon.o: callscreen.h calls.h modem-ofono.h
modem-ofono.o: calls.h modem-ofono.h

%.o: %.c
	$(CC) -o $@ -c $*.c $(CFLAGS)

clean:
	rm -f $(OBJECTS) $(DIALOBJECTS) $(OUTPUTS)

install:
	install $(OUTPUTS) $(DESTDIR)/usr/bin
	install ringer.sh ringer.ogg $(DESTDIR)/usr/share/bananui
